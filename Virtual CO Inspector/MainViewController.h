//
//  MainViewController.h
//  Virtual CO Inspector
//
//  Created by Mark Rudolph on 12/17/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MainViewController : BaseViewController <AVAudioPlayerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *coLabel;

@property (weak, nonatomic) IBOutlet UIImageView *led1;
@property (weak, nonatomic) IBOutlet UIImageView *led2;
@property (weak, nonatomic) IBOutlet UIImageView *led3;
@property (weak, nonatomic) IBOutlet UIImageView *led4;


@property (weak, nonatomic) IBOutlet UIButton *pwrBtn;
@property (weak, nonatomic) IBOutlet UIButton *maxBtn;

@property (weak, nonatomic) IBOutlet UIImageView *muteArrow;
@property (weak, nonatomic) IBOutlet UIImageView *maxArrow;

@property AVAudioPlayer *alarm;

@property float currentOffset;

@property BOOL isMaxDown;
@property BOOL isPowerDown;

@property BOOL isInMaxMode;
@property BOOL isInMuteMode;

@property float maxValue;

@property BOOL isInLowAlarmState;
@property BOOL isInHighAlarmState;

@property NSMutableArray *baselineArray;
@property BOOL isCalibrating;

@property NSTimer *repeatingMeasure;
@property NSTimer *calibrationCountdown;

@property UILongPressGestureRecognizer *longPress;
-(void)doOnLongPress:(UILongPressGestureRecognizer*)sender;

@property UILongPressGestureRecognizer *longPressDummy;
-(void)doOnDummyLongPress:(UILongPressGestureRecognizer*)sender;


- (IBAction)maxDOWN:(id)sender;
- (IBAction)maxUP:(id)sender;

- (IBAction)pwrDOWN:(id)sender;
- (IBAction)pwrUP:(id)sender;

-(void)blinkLEDs;

-(void)setMute:(BOOL)onOff;
-(void)setMax:(BOOL)onOff;

-(BOOL)connectionCheck;
-(void)doCalibration;
@end
