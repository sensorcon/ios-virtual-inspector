//
//  PreferenceViewController.m
//  Virtual CO Inspector
//
//  Created by Mark Rudolph on 12/27/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "PreferenceViewController.h"

@interface PreferenceViewController ()

@end

@implementation PreferenceViewController

@synthesize offsetLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    float currentOffset = [[NSUserDefaults standardUserDefaults] floatForKey:@"OFFSET"];
    [offsetLabel setText:[NSString stringWithFormat:@"%.0f", currentOffset]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)resetOffset:(id)sender {
    [[NSUserDefaults standardUserDefaults] setFloat:0 forKey:@"OFFSET"];
    [offsetLabel setText:[NSString stringWithFormat:@"%.0f", 0.0]];
    float currentOffset = [[NSUserDefaults standardUserDefaults] floatForKey:@"OFFSET"];
    [offsetLabel setText:[NSString stringWithFormat:@"%.0f", currentOffset]];
}
@end
