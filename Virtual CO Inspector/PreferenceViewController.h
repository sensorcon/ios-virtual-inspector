//
//  PreferenceViewController.h
//  Virtual CO Inspector
//
//  Created by Mark Rudolph on 12/27/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface PreferenceViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *offsetLabel;

- (IBAction)resetOffset:(id)sender;
@end
