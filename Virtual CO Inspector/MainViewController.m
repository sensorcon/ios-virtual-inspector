//
//  MainViewController.m
//  Virtual CO Inspector
//
//  Created by Mark Rudolph on 12/17/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize led1;
@synthesize led2;
@synthesize led3;
@synthesize led4;

@synthesize maxBtn;
@synthesize pwrBtn;

@synthesize muteArrow;
@synthesize maxArrow;

@synthesize isMaxDown;
@synthesize isPowerDown;

@synthesize isInMuteMode;
@synthesize isInMaxMode;

@synthesize repeatingMeasure;
@synthesize coLabel;
@synthesize longPress;
@synthesize longPressDummy;

@synthesize isCalibrating;
@synthesize baselineArray;

@synthesize isInLowAlarmState;
@synthesize isInHighAlarmState;

@synthesize currentOffset;
@synthesize maxValue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    [coLabel setFont:[UIFont fontWithName:@"DS-DIGI" size:coLabel.font.pointSize]];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"beep" ofType:@"mp3"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
    self.alarm = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:NULL];
    [self.alarm setVolume:1];
    [self.alarm setDelegate:self];
    [self.alarm prepareToPlay];
    
    [self blinkLEDs];
    [self setMute:NO];
    [self setMax:NO];
    
    self.isPowerDown = NO;
    self.isMaxDown = NO;
    self.isInLowAlarmState = NO;
    self.isInHighAlarmState = NO;
    
    maxValue = 0;
    
    baselineArray = [[NSMutableArray alloc] init];

    
    // Set up our long press handler
    longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(doOnLongPress:)];
    longPress.minimumPressDuration = 2.0;
    [pwrBtn addGestureRecognizer:longPress];
    
    longPressDummy = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(doOnDummyLongPress:)];
    longPressDummy.minimumPressDuration = 2.0;
    [maxBtn addGestureRecognizer:longPressDummy];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if (![self connectionCheck]) {
        [coLabel setText:@"Off"];
    }
    
    currentOffset = [[NSUserDefaults standardUserDefaults] floatForKey:@"OFFSET"];

    // Reset this when the view appears
    isCalibrating = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)maxDOWN:(id)sender {
    self.isMaxDown = YES;
    // is pwr down too?
    
    if (![self connectionCheck]) {
        [self notifyNotConnected];
        // This will steal focus
        [self setMute:NO];
    }
}

- (IBAction)maxUP:(id)sender {
    self.isMaxDown = NO;

    if (self.isInMaxMode) {
        [self setMax:NO];
    } else {
        [self setMax:YES];
    }
 
}

- (IBAction)pwrDOWN:(id)sender {
    self.isPowerDown = YES;
    
    if (![self connectionCheck]) {
        [self notifyNotConnected];
        // This will steal focus
        [self setMute:NO];
    }
}

- (IBAction)pwrUP:(id)sender {
    self.isPowerDown = NO;
    
    // This turns off the device
    // This will turn on the device
    if ([self connectionCheck]) {
        if ([self.delegate.myDrone precisiongGasStatus]) {
            // If we're already enabled,
            // We must want to handle Mutes...
            if (self.isInMuteMode) {
                [self setMute:NO];
                self.isInMuteMode = NO;
            }
            else {
                [self setMute:YES];
                self.isInMuteMode = YES;
            }
        }
        else {
            [self.delegate.myDrone enablePrecisionGas];
        }
    }
}

-(void)blinkLEDs {
    // Start with all of the LEDs off
    [led1 setImage:[UIImage imageNamed:@"led_off.png"]];
    [led2 setImage:[UIImage imageNamed:@"led_off.png"]];
    [led3 setImage:[UIImage imageNamed:@"led_off.png"]];
    [led4 setImage:[UIImage imageNamed:@"led_off.png"]];
    NSTimer *blinkTimer;
    blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(phase1) userInfo:nil repeats:NO];
    // Do the beeping here...
    if (self.isInLowAlarmState || self.isInHighAlarmState) {
        if (!self.isInMuteMode) {
            [self.alarm play];
        }
    }
}


-(void)phase1 {
    [led1 setImage:[UIImage imageNamed:@"led_on.png"]];
    NSTimer *blinkTimer;
    blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(phase2) userInfo:nil repeats:NO];
}

-(void)phase2 {
    [led1 setImage:[UIImage imageNamed:@"led_off.png"]];
    [led2 setImage:[UIImage imageNamed:@"led_on.png"]];
    NSTimer *blinkTimer;
    blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(phase3) userInfo:nil repeats:NO];
    
}

-(void)phase3 {
    [led2 setImage:[UIImage imageNamed:@"led_off.png"]];
    [led3 setImage:[UIImage imageNamed:@"led_on.png"]];
    NSTimer *blinkTimer;
    blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(phase4) userInfo:nil repeats:NO];
}

-(void)phase4 {
    [led3 setImage:[UIImage imageNamed:@"led_off.png"]];
    [led4 setImage:[UIImage imageNamed:@"led_on.png"]];
    NSTimer *blinkTimer;
    blinkTimer = [NSTimer scheduledTimerWithTimeInterval:0.20 target:self selector:@selector(phase5) userInfo:nil repeats:NO];
    
}

-(void)phase5 {
    [led4 setImage:[UIImage imageNamed:@"led_off.png"]];
}


-(void)setMax:(BOOL)onOff {
    self.isInMaxMode = onOff;
    maxArrow.hidden = !onOff;
//    if (!onOff) {
        maxValue = 0;
//    }
}

-(void)setMute:(BOOL)onOff {
    self.isInMuteMode = onOff;
    muteArrow.hidden = !onOff;
}



-(BOOL)connectionCheck {
    if ([self.delegate.myDrone.dronePeripheral state] == CBPeripheralStateConnected) {
        return YES;
    } else {
        return NO;
    }
}



-(void)notifyNotConnected {
    [[[UIAlertView alloc] initWithTitle:@"Not connected!" message:@"It seems you are not currently connected to your Sensordrone. Please connect from the connections page first." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}


-(void)doOnPrecisonGasEnabled {
    repeatingMeasure = [NSTimer scheduledTimerWithTimeInterval:1 target:self.delegate.myDrone selector:@selector(measurePrecisionGas) userInfo:nil repeats:YES];
}

-(void)doOnPrecisonGasDisabled {
    if (repeatingMeasure != nil) {
        [repeatingMeasure invalidate];
        repeatingMeasure = nil;
    }
    [self setMute:NO];
    [self setMax:NO];
    [coLabel setText:@"Off"];
}

-(void)doOnPrecisonGasMeasured {
    // For now, add in a bit of randomness to help with debugging
    
    // Are we calibrating?
    if ([self isCalibrating]) {
        if ([baselineArray count] < 10) {
            [baselineArray addObject:[[NSNumber alloc] initWithFloat:self.delegate.myDrone.measuredPrecisionGasInPPMCO]];
            [coLabel setText:[NSString stringWithFormat:@"CAL %0lu/10", (unsigned long)[baselineArray count]]];
        } else {
            //We're done!
            // Store the average, clear the data, and set the flag
            [self calculateBaseline];
            self.isCalibrating = NO;
        }
    }
    else {
        // If we're not calibrating, then we're showing data
        // Set any alarms
        float coValue = self.delegate.myDrone.measuredPrecisionGasInPPMCO;
        // Offset
        // It has already been negated, so ADD it.
        coValue += currentOffset;
        
        if (self.isInMaxMode) {
            if (coValue > maxValue) {
                maxValue = coValue;
            }
        }
        
        
        if (coValue >= 35 && coValue < 200) {
            self.isInLowAlarmState = YES;
            self.isInHighAlarmState = NO;
            [self blinkLEDs];
        }
        else if (coValue >= 200) {
            self.isInLowAlarmState = NO;
            self.isInHighAlarmState = YES;
            [self blinkLEDs];
        }
        else {
            self.isInLowAlarmState = NO;
            self.isInHighAlarmState = NO;
        }
        
        if (self.isInMaxMode) {
            [coLabel setText:[NSString stringWithFormat:@"%.0f ppm", maxValue]];
        } else {
            // Don't show negative 0 (due to minor differences in offsets).
            if (coValue < 0 &&  coValue > -1) {
                coValue = abs(0);
            }
            [coLabel setText:[NSString stringWithFormat:@"%.0f ppm", coValue]];
        }
        
    }
}

-(void)doOnLongPress:(UILongPressGestureRecognizer *)sender {
    // Don't do anything if not conencted
    if (![self connectionCheck] || ![self.delegate.myDrone precisiongGasStatus]) {
        return;
    }
    
    // This fires if we've made it longer than required
    if (sender.state == UIGestureRecognizerStateEnded) {
        //Do Whatever You want on End of Gesture
        
        // Were both buttos held down?
        
        // If not, we just want to turn the device off
        
        if (self.isPowerDown && self.isMaxDown) {
            [self doCalibration];
        }
        else {
            [self.delegate.myDrone disablePrecisionGas];
        }
    }
}

-(void)doOnDummyLongPress:(UILongPressGestureRecognizer *)sender {
    if (![self connectionCheck]) {
        return;
    }
    if (sender.state == UIGestureRecognizerStateEnded) {
        // Just want to catch here to prevent toggling of Max mode while trying to calibrate
        // May not need this if doCalibration steals foucs
        // But we will need to reset the state
    }

}

-(void)doCalibration {
    isCalibrating = YES;
    // Reset this, just in case
    self.isMaxDown = NO;

}

// This method stores a baseline in a shared preferences
// Store the NEGATIVE so you only need to ADD to readings.
-(void)calculateBaseline {
    float sum = 0;
    for (int i = 0; i < [baselineArray count]; i++) {
        sum += [[baselineArray objectAtIndex:i] floatValue];
    }
    float average = -1 * sum / [baselineArray count];
    [[NSUserDefaults standardUserDefaults] setFloat:average forKey:@"OFFSET"];
    currentOffset = average;
    // Store the baseline
    // Clear our array for next time
    [baselineArray removeAllObjects];
}

@end
